#!/usr/bin/awk -f

# reads from stdin. 
# Usage: $ ./bigcols.awk < input1.tsv

# from stackoverflow where he wanted to find rows
# where 50% of the values were bigger than the input
# var.
# this was the accepted solution
#
#  awk -v t=5 '{c=0; for(i=1;i<=NF;i++) c+=($i>t)} c/NF>0.5' file



# Run at start.
BEGIN { 
#	print "Start" 
#	print "TSV setting. Field seperator set to tab."
	FS = "\t"
	# He wants to find lines with avg greater than var x
	x=5
}

# main. Run for each record. This code uses newlines to denote records.
{
	# Find lines which are of this form: (skip header)
	# #+,
	# ie. start with one or more numbers in column 1.
	if ($1 ~ /^[0-9]+/) {
		the_avg = ($1 + $2 + $3 + $4 + $5)/5
		if (the_avg > x) {
		    print $1, $2, $3, $4, $5
		}
	}
}

# run at end
#END { print "Stop" }

