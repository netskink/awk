# Awk follow along

## Specify the field seperator and print column 1 of /etc/passwd

```
awk -F ":" '{print $1}' /etc/passwd
```

`$0` is all the columns

## Manipulate the columns

```
awk -F ":" '{print $1 $6 $7}' /etc/passwd
awk -F ":" '{print $1" "$6" "$7}' /etc/passwd
awk -F ":" '{print $1"\t"$6"\t"$7}' /etc/passwd
#
# Adding a BEGIN block and an output field seperator
#Notice, no quotes in print and use of commas
awk 'BEGIN{FS=":"; OFS="-"} {print $1, $6, $7}' /etc/passwd
```

## Manipulate the rows using search

Only manipulate lines which match the search

```
awk -F "/" '/some_pattern/' some_file
```

### dump the path names from /etc/shells without printing any comments

* ^ is from beginning of line
* / need to be escaped with \
* $NF is number of fields.  The first column/field is $1

```
awk -F "/" '/^\// {print $NF}' /etc/shells | uniq | sort

```

## Can do math with the variables

```
df | awk '/\/dev\/nvm/ {print $1"\t"$4"\t"$2}'
df | awk '/\/dev\/nvm/ {print $1"\t"$4+$2}'
```

Lines greater than seven characters

```
awk 'length($0) > 7' /etc/shells
```

```
awk 'BEGIN { for(i=1; i<=10; i++) print "The square root of", i, "is", i*i;}'

```

print any lines in .bashrc, which have the first column, start with a `b` or `c`.

```
awk '$1 ~ /^[b,c]/ {print $0}' ~/.bashrc

```

Print all but first four characters of each line

```
$ awk '{print substr($0, 4)}' /etc/passwd
```

RSTART index location of where match occurs
```
$ awk 'match($0, /o/) {print $0 " has \"o\" character at " RSTART}' /etc/passwd
```

NR is line number
```
$ df | awk 'NR==7, NR==11 {print NR, $0}'
```

Perhps he is matching on the last line, and thus printing out the number of line?

```
$ awk 'END {print NR}' /etc/shells
```

It will also sum automatically


```
$ awk 'END {print NR}' /etc/shells /etc/shells
```









